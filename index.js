addEventListener('fetch', event => {
  event.respondWith(handleRequest(event.request))
})

async function returnScript(name) {
return `
window.addEventListener('DOMContentLoaded', function() {
  if (window.requestIdleCallback) {
    requestIdleCallback(function () {
       hhh();
    })
  } else {
    setTimeout(function () {
     hhh();
    }, 500)
  }
});

function hhh() {
  ${name}.get(options, function (components) {
    var values = components.map(function (component) { return component.value })
    var murmur = ${name}.x64hash128(values.join(''), 31);
    console.log(murmur);
  })
}
`;
}
/**
 * Respond with hello worker text
 * @param {Request} request
 */
function name() {
  return Math.random().toString(36).slice(2);
}
async function handleRequest(request) {
  const r = await fetch('https://raw.githubusercontent.com/Valve/fingerprintjs2/master/fingerprint2.js');
  const body = await r.text();
  let name = name();
  let k = body.split("Fingerprint2").join(name);
  let g = k + returnScript(name);
  return new Response(g, {
    headers: { 'content-type': 'text/plain' },
  })
}
